/*
** functions.c for magic 21 in /home/grotzk_a/rendu/Magic21
** 
** Made by grotzk_a
** Login   <grotzk_a@epitech.net>
** 
** Started on  Sun Oct 12 17:49:10 2014 grotzk_a
** Last update Sat Oct 25 13:25:07 2014 grotzk_a
*/

#include "nintendo.h"

void	my_putchar(char c)
{
  write(1, &c, 1);
}

void	my_putstr(char *str)
{
  int	i;

  i = 0;
  while (str[i] != '\0')
    my_putchar(str[i++]);
}

void	my_put_nbr(int nb)
{
  int	neg;

  neg = 0;
  if (nb < 0)
    {
      my_putchar('-');
      if (nb == -2147483648)
	{
	  neg = 1;
	  nb++;
	}
      nb = nb * -1;
    }
  if (nb >= 10)
    my_put_nbr(nb / 10);
  if (neg == 1)
    {
      neg = 0;
      my_putchar(nb % 10 + '1');
    }
  else
    my_putchar(nb % 10 + '0');
}

