/*
** nintendo.h for MarioKart in /home/grotzk_a/rendu/ReRush1/sources
** 
** Made by grotzk_a
** Login   <grotzk_a@epitech.net>
** 
** Started on  Sat Oct 25 12:31:17 2014 grotzk_a
** Last update Sat Oct 25 15:05:57 2014 grotzk_a
*/

#ifndef NINTENDO_H_
#define NINTENDO_H_

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <ncurses.h>
#include <time.h>
#include <sys/types.h>
#include <fcntl.h>

#define WALL    #
#define ROAD    %

int     my_strlen(const char *);
int     my_fputchar(const int fd, const char);
int     my_fputstr(const int fd, const char *);
void	my_putchar(char);
void	my_putstr(char *);
char    *my_strdup(const char *);
int     my_strcmp(const char *, const char *);
int	my_power_rec(int, int);
long	power_n(long);
int	my_getnbr(char *s);
char	**generate_tab(int, int, char **);
void	print_tab(char **, int, int);

#endif  /* !NINTENDO_H_  */
