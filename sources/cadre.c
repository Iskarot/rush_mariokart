/*
** cadre.c for MarioKart in /home/grotzk_a/rendu/ReRush1/sources
** 
** Made by grotzk_a
** Login   <grotzk_a@epitech.net>
** 
** Started on  Sat Oct 25 12:18:43 2014 grotzk_a
** Last update Sat Oct 25 15:26:33 2014 grotzk_a
*/

#include "nintendo.h"

char		**generate_tab(int x, int y, char **tab)
{
  int		i;

  i = 0;
  if ((tab = malloc(sizeof(char) * x)) == NULL)
    {
      my_putstr("Malloc error.\n");
      exit(0);
    }
  while (i <= x)
    {
      if ((tab[i] = malloc(sizeof(char) * y)) == NULL)
        {
          my_putstr("Malloc error.\n");
          exit(0);
        }
      i = i + 1;
    }
  return (tab);
}

void		print_border(int y)
{
  int	i;

  i = 0;
  while (i < y + 2)
    {
      my_putchar('#');
      i++;
    }
  my_putchar('\n');
}

void		print_tab(char **tab, int x, int y)
{
  int	i;
  int	j;

  print_border(y);
  i = 0;
  while (i < x)
    {
      j = 0;
      my_putchar('#');
      while (j < y)
	{
	  my_putchar(' '/*tab[i][j]*/);
	  j++;
	}
      my_putchar('#');
      my_putchar('\n');
      i++;
    }
  print_border(y);
}
