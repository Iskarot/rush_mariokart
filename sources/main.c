/*
** main.c for MarioKart in /home/grotzk_a/rendu/ReRush1/sources
** 
** Made by grotzk_a
** Login   <grotzk_a@epitech.net>
** 
** Started on  Sat Oct 25 12:15:44 2014 grotzk_a
** Last update Sat Oct 25 15:51:26 2014 grotzk_a
*/

#include "nintendo.h"

int		main(int argc, char **argv)
{
  int		x;
  int		y;
  char		**tab;

  if (argc != 3)
    {
      my_putstr("Usage : ./MarioKart [hauteur] [largeur]\n\n"); 
      return (0);
    }
  y = my_getnbr(argv[1]);
  x = my_getnbr(argv[2]);
  tab = NULL;
  if ((tab = generate_tab(x, y, tab)) == NULL)
    {
      my_putstr("Tab generation failed \n");
      return (0);
    }
  print_tab(tab, x, y);
  return (0);
}
