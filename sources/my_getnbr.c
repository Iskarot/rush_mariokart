/*
** my_getnbr.c for MarioKart in /home/grotzk_a/rendu/RushMKart/sources
** 
** Made by grotzk_a
** Login   <grotzk_a@epitech.net>
** 
** Started on  Sat Oct 25 13:39:25 2014 grotzk_a
** Last update Sat Oct 25 13:39:34 2014 grotzk_a
*/

#include "nintendo.h"

int     my_power_rec(int nb, int p)
{
  if (p > 1)
    nb = nb * my_power_rec(nb, p - 1);
  else nb = 1;
}

long    power_n(long n)
{
  long  i;
  long  out;

  i = 1;
  out = 1;
  while (i < n)
    {
      out = out * 10;
      i = i + 1;
    }
  return (out);
}

int     my_getnbr(char *s)
{
  int   i;
  int   out;
  int   neg;

  i = 0;
  out = 0;
  neg = 1;
  if (s[i] == '-')
    {
      neg = -1;
      i++;
    }
  while (s[i] && s[i] >= '0' && s[i] <= '9')
    {
      out = out * 10;
      out += s[i] - 48;
      i++;
    }
  return (neg * out);
}
